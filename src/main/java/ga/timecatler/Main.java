package ga.timecatler;

import ga.timecatler.Core.AuthorScrapper;
import ga.timecatler.Core.IScrapper;
import ga.timecatler.Storage.DocxFile;
import ga.timecatler.Storage.IFile;
import ga.timecatler.Types.Poem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;

@CommandLine.Command(description = "Scraps poems for author page from stihi.ru.",
        name = "StihiScrapper", mixinStandardHelpOptions = true, version = "StihiScrapper 0.2")
public class Main implements Callable<Integer> {
    @CommandLine.Spec
    private CommandLine.Model.CommandSpec spec;

    @Option(names = {"-v", "--verbose"}, description = "Verbose mode.")
    public void setVerboseMode(boolean mode) {
        Config.Constants.VERBOSE_MODE = mode;
    }

    @Option(names = {"--debug"}, description = "Use debug mode (load only first 50 poems)")
    public void setDebugMode(boolean mode) {
        Config.Constants.DEBUG_MODE = mode;
    }

    @Option(names = {"-a", "--author-format"}, description = "Generate output file with some additional links "
            + "useful for authors only")
    public void setAuthorRepresentation(boolean mode) {
        Config.Constants.AUTHOR_REPRESENTATION = mode;
    }

    @Option(names = {"-p"}, defaultValue = "false", description = "Use base domain proza.ru instead of stihi.ru")
    public void setBaseDomain(boolean is_proza)  { Config.Constants.BASE_DOMAIN = is_proza ? "proza.ru" : "stihi.ru"; }

    @Option(names = {"-r", "--retries"}, defaultValue = "5", description = "Number of retries for page download")
    public void setNumRetries(Integer num_retries) { Config.Constants.NUM_RETRIES = num_retries; }

    @Option(names = {"-t", "--timeout"}, defaultValue = "100", description = "Initial interval for attempt at redownloading the page (ms)")
    public void setTimeout(Integer timeout) { Config.TIMEOUT = new Config.Timeout(timeout); }

    @Option(names = {"-o", "--output"}, arity = "1", description = "Output file path")
    private Path output;

    private static boolean isValidAuthorLink(String link) throws IOException {
        Document page = Jsoup.connect(link).get();
        Elements selected = page.select("h1");
        return !selected.first()
                .childNodes().get(0)
                .toString().equals("Автор не найден");
    }

    @Parameters(index = "0", arity = "1", description = "Author page link or account name")
    private String author = "";
    private String author_link = "";

    /**
     * Validates 'author' parameter and sets the correct author link or throws error message
     * indicating problems with parameter
     *
     */
    public void validateAuthor() {
        String domain = Config.Constants.BASE_DOMAIN;
        String urlRegex = "(https?://)?(www.)?" + domain + "/author/\\w+";
        String authorRegex = "\\w+";
        // When input seems legit
        if (author.matches(authorRegex) || author.matches(urlRegex)) {
            String authorLink;

            String baseUri = "https://" + domain + "/author/";
            // If only author add prefix otherwise use directly
            if (author.matches(authorRegex))
                authorLink = baseUri + author;
            else
                authorLink = author;

            // If you can't connect to internet Jsoup throws an exception
            // Then we catch it and throw exception of our own (handled by picocli)
            try {
                // If author page doesn't exist, throw exception (handled by picocli)
                if (isValidAuthorLink(authorLink))
                    author_link = authorLink;
                else
                    throw new CommandLine.ParameterException(spec.commandLine(),
                            String.format("Supplied <author> argument '%s' doesn't specify a valid author page at '%s'.",
                                    author, authorLink));
            } catch (IOException e) {
                throw new CommandLine.ParameterException(spec.commandLine(),
                        String.format("Unable to connect to internet: %s", e.toString()));
            }
        } else {
            throw new CommandLine.ParameterException(spec.commandLine(),
                    String.format("Supplied <author> argument '%s' is not a valid input.", author));
        }
    }

    @Override
    public Integer call() {
        validateAuthor();

        IScrapper<List<Poem>> scrapper = new AuthorScrapper();
        scrapper.loadPage(author_link);
        IFile file = new DocxFile();
        file.fromResult(scrapper.presentResult());
        file.saveToPath(output.toAbsolutePath().toString());
        return 0;
    }

    public static void main(String[] args) {
        System.exit(new CommandLine(new Main()).execute(args));
    }
}
