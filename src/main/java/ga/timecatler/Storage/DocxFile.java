package ga.timecatler.Storage;

import com.sun.istack.NotNull;
import ga.timecatler.Config;
import ga.timecatler.Types.Poem;

import org.docx4j.dml.wordprocessingDrawing.Inline;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.relationships.Namespaces;
import org.docx4j.wml.Drawing;
import org.docx4j.wml.ObjectFactory;
import org.docx4j.wml.P;
import org.docx4j.wml.P.Hyperlink;
import org.docx4j.XmlUtils;
import org.docx4j.wml.R;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Docx file
 */
public class DocxFile implements IFile {

    private WordprocessingMLPackage wordMLPackage;
    private MainDocumentPart doc;

    public DocxFile() {
        try {
            wordMLPackage = WordprocessingMLPackage.createPackage();
            doc = wordMLPackage.getMainDocumentPart();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds the paragraph with a subtitle style to document
     *
     * @param text What to add
     */
    private void addSubtitleParagraph(String text) {
        doc.addStyledParagraphOfText("Subtitle", text);
    }

    /**
     * Adds the paragraph with a default text style to document
     *
     * @param text What to add
     */
    private void addParagraph(String text) {
        doc.addParagraphOfText(text);
    }

    /**
     * Creates hyperlink from url with text.
     * Courtesy of <a href = https://github.com/plutext/docx4j/blob/master/src/samples/docx4j/org/docx4j/samples/HyperlinkTest.java>Docx4J examples</a>
     * @param doc Document for hyperlink relationship
     * @param url Url
     * @param text Link text
     * @return Hyperlink
     */
    private static Hyperlink createHyperlink(MainDocumentPart doc, String url, String text) {
        try {
            // We need to add a relationship to word/_rels/document.xml.rels
            // but since its external, we don't use the
            // usual wordMLPackage.getMainDocumentPart().addTargetPart
            // mechanism
            org.docx4j.relationships.ObjectFactory factory =
                    new org.docx4j.relationships.ObjectFactory();

            org.docx4j.relationships.Relationship rel = factory.createRelationship();
            rel.setType( Namespaces.HYPERLINK  );
            rel.setTarget(url);
            rel.setTargetMode("External");

            doc.getRelationshipsPart().addRelationship(rel);

            // addRelationship sets the rel's @Id

            String hpl = "<w:hyperlink r:id=\"" + rel.getId() + "\" xmlns:w=\"http://schemas.openxmlformats.org/wordprocessingml/2006/main\" " +
                    "xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" >" +
                    "<w:r>" +
                    "<w:rPr>" +
                    "<w:rStyle w:val=\"Hyperlink\" />" +  // TODO: enable this style in the document!
                    "</w:rPr>" +
                    "<w:t>" + text + "</w:t>" +
                    "</w:r>" +
                    "</w:hyperlink>";
            return (Hyperlink)XmlUtils.unmarshalString(hpl);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Creates hyperlink from url.
     * Uses {@link #createHyperlink(MainDocumentPart, String, String)} inside.
     * @param doc Document for hyperlink relationship
     * @param url Url
     * @return Hyperlink
     */
    private static Hyperlink createHyperlink(MainDocumentPart doc, String url) {
        return createHyperlink(doc, url, url);
    }

    private void addPoemUrl(String url) {
        addUrlWithText(url, url);
    }

    private void addUrlWithText(String url, String text) {
        Hyperlink link = createHyperlink(doc, url, text);
        doc.addParagraphOfText("").getContent().add(link);
    }

    private void addImage(@NotNull File img) {
        try {
            BinaryPartAbstractImage imgPart = BinaryPartAbstractImage.createImagePart(wordMLPackage,img);
            Inline inlineImg = imgPart.createImageInline(img.getName(), "Image", 0,1, false);
            doc.addObject(addInlineImageToParagraph(inlineImg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static P addInlineImageToParagraph(Inline inline) {
        // Now add the in-line image to a paragraph
        ObjectFactory factory = new ObjectFactory();
        P paragraph = factory.createP();
        R run = factory.createR();
        paragraph.getContent().add(run);
        Drawing drawing = factory.createDrawing();
        run.getContent().add(drawing);
        drawing.getAnchorOrInline().add(inline);
        return paragraph;
    }

    /**
     * Adds a poem to document
     *
     * @param poem What to add
     */
    private void addPoem(Poem poem) {
        addSubtitleParagraph(poem.getHeader());
        if (poem.getImage() != null)
            addImage(poem.getImage());
        Arrays.asList(poem.getText().split("\\r?\\n")).forEach(this::addParagraph);
        addParagraph("");
        if (Config.Constants.AUTHOR_REPRESENTATION) {
            addAuthorLinks(poem);
        }
        addParagraph("\r\n");
    }

    private void addAuthorLinks(Poem poem) {
        String editText = "Редактировать.";
        String moveText = "Переместить.";
        String announceText = "Анонсировать";
        String readersText = "Читатели";

        String domain = Config.Constants.BASE_DOMAIN;
        String prefix = "https://" + domain + "/";
        String poemPath = poem.getURL().replaceFirst("(https?://)?(www.)?" + domain + "/avtor/", "");
        String editLink = prefix + "page.html?edit&link=" + poemPath;
        String moveLink = prefix + "page.html?move&link=" + poemPath;
        String announceLink = prefix + "promo.html?anons&link=" + poemPath;
        String readersLink = prefix + "readers.html?" + poemPath;

        addPoemUrl(poem.getURL());
        addUrlWithText(editLink, editText);
        addUrlWithText(moveLink, moveText);
        addUrlWithText(announceLink, announceText);
        addUrlWithText(readersLink, readersText);
    }

    /**
     * Saves current document at path
     *
     * @param path Where to save file (with filename and extension)
     * @throws Docx4JException Thrown by {@link WordprocessingMLPackage#save(File)}
     */
    private void saveTo(String path) throws Docx4JException {
        // TODO: check if file already exists
        wordMLPackage.save(new java.io.File(path));
        System.out.println("Saved to " + path);
    }

    /**
     * Loads result to a file
     *
     * @param result List of scrapped poems
     */
    @Override
    public void fromResult(List<Poem> result) {
        result.forEach(this::addPoem);
    }

    /**
     * Saves file to path
     *
     * @param path Where to save file (with filename and extension)
     */
    @Override
    public void saveToPath(String path) {
        try {
            saveTo(path);
        } catch (Docx4JException e) {
            e.printStackTrace();
        }
    }

}
