package ga.timecatler.Storage;

import ga.timecatler.Types.Poem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Plain text file
 */
public class TextFile implements IFile {
    private List<String> buffer = new ArrayList<>();

    /**
     * Converts list of poems to list of strings
     *
     * @param result List of poems to convert
     * @return List of strings
     */
    private static List<String> convert(List<Poem> result) {
        List<String> convertedList = new ArrayList<>();

        for (Poem poem : result) {
            convertedList.add("");
            convertedList.add(poem.getHeader());
            convertedList.addAll(Arrays.asList(poem.getText().split("\\r?\\n")));
            convertedList.add(poem.getURL());
        }
        return convertedList;
    }

    /**
     * Writes list of strings in plaintext file at path
     *
     * @param path   Where to write
     * @param source What to write
     * @throws IOException Thrown by {@link File#createNewFile}
     */
    private void writeStrings(String path, List<String> source) throws IOException {
        File file = new File(path);
        file.getParentFile().mkdirs();
        if (!file.exists())
            file.createNewFile();

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);

        for (String line : source) {
            bw.write(line);
            bw.newLine();
        }
        bw.close();
        fw.close();
    }

    /**
     * Converts poems to list of strings and saves the to private buffer
     *
     * @param result List of scrapped poems
     */
    @Override
    public void fromResult(List<Poem> result) {
        buffer.addAll(convert(result));
    }


    /**
     * Writes private buffer at path
     *
     * @param path Where to save file (with filename and extension)
     */
    @Override
    public void saveToPath(String path) {
        try {
            writeStrings(path, buffer);
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}
