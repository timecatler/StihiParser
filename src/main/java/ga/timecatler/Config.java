package ga.timecatler;

public class Config {
    public static Timeout TIMEOUT;

    public static class Timeout
    {
        private Integer current_value;

        public Timeout(Integer initial_value)  { this.current_value = initial_value; }

        public Integer value()  { return current_value; }

        public void increase_value()  { current_value = current_value*2; }
    }
    public static class Constants {
        public static boolean DEBUG_MODE;
        public static boolean AUTHOR_REPRESENTATION;
        public static boolean VERBOSE_MODE;
        public static String BASE_DOMAIN;
        public static Integer NUM_RETRIES;
    }
}
