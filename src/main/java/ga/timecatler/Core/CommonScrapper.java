package ga.timecatler.Core;

import ga.timecatler.Config;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Abstract class, containing default implementation of a {@link IScrapper#loadPage} method
 *
 * @param <T> Result type
 */
public abstract class CommonScrapper<T> implements IScrapper<T> {
    Document webpage;

    /**
     * Loads page using {@link Jsoup#connect}
     *
     * @param url URL
     */
    @Override
    public void loadPage(String url) {
        try {
            for (int i = 1; i <= Config.Constants.NUM_RETRIES; i++) {
                try {
                    // TODO: Replace with logging
                    if (Config.Constants.VERBOSE_MODE)
                        System.out.printf(
                                "Loading page from %s%s%n",
                                url,
                                i == 1 ? "" : String.format(": attempt %d/%d", i, Config.Constants.NUM_RETRIES)
                        );
                    webpage = Jsoup.connect(url).get();
                    return;
                } catch (HttpStatusException e) {
                    if (e.getStatusCode() != 503) { throw e; }
                    // TODO: Replace with logging
                    if (Config.Constants.VERBOSE_MODE)
                        System.out.printf(
                                "Loading page from %s: attempt %d/%d: Failure, retrying in %dms%n",
                                url,
                                i,
                                Config.Constants.NUM_RETRIES, Config.TIMEOUT.value()
                        );
                    Config.TIMEOUT.increase_value();
                    Thread.sleep(Config.TIMEOUT.value());
                }
            }
        } catch (IOException | InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    abstract public T presentResult();
}
